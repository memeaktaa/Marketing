<div id="side-panel">

    <div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

    <div class="side-panel-wrap">

        <div class="widget clearfix">

            <h4>Pages</h4>

            <nav class="nav-tree nobottommargin">
                <ul>
                    <li><a href="#"><i class="icon-bolt2"></i>Features</a>
                        <ul>
                            <li><a href="#">Sliders</a></li>
                            <li><a href="#">Widgets</a></li>
                            <li><a href="#">Events</a></li>
                            <li><a href="#">Headers</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-briefcase"></i>Portfolio</a>
                        <ul>
                            <li><a href="#">Grid</a>
                                <ul>
                                    <li><a href="#">3 Columns</a></li>
                                    <li><a href="#">4 Columns</a></li>
                                    <li><a href="#">5 Columns</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Masonry</a></li>
                            <li><a href="#">Loading Styles</a></li>
                            <li><a href="#">Single</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-file-text"></i>About</a>
                        <ul>
                            <li><a href="#">Company</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">FAQs</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="icon-map-marker"></i>Contact</a></li>
                </ul>
            </nav>

        </div>
    </div>

</div>