<footer class="section page-footer page-footer-extended text-center text-md-left bg-gray-darker">
    <div class="rd-google-map-wrap">
        <div class="rd-google-map rd-google-map-novi rd-google-map__model" data-zoom="15" data-y="40.643180"
             data-x="-73.9874068"
             data-styles="[ { &quot;featureType&quot;: &quot;all&quot;, &quot;elementType&quot;: &quot;labels.text.fill&quot;, &quot;stylers&quot;: [ { &quot;saturation&quot;: 36 }, { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 40 } ] }, { &quot;featureType&quot;: &quot;all&quot;, &quot;elementType&quot;: &quot;labels.text.stroke&quot;, &quot;stylers&quot;: [ { &quot;visibility&quot;: &quot;on&quot; }, { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 16 } ] }, { &quot;featureType&quot;: &quot;all&quot;, &quot;elementType&quot;: &quot;labels.icon&quot;, &quot;stylers&quot;: [ { &quot;visibility&quot;: &quot;off&quot; } ] }, { &quot;featureType&quot;: &quot;administrative&quot;, &quot;elementType&quot;: &quot;geometry.fill&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 20 } ] }, { &quot;featureType&quot;: &quot;administrative&quot;, &quot;elementType&quot;: &quot;geometry.stroke&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 17 }, { &quot;weight&quot;: 1.2 } ] }, { &quot;featureType&quot;: &quot;landscape&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 20 } ] }, { &quot;featureType&quot;: &quot;poi&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 21 } ] }, { &quot;featureType&quot;: &quot;road.highway&quot;, &quot;elementType&quot;: &quot;geometry.fill&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 17 } ] }, { &quot;featureType&quot;: &quot;road.highway&quot;, &quot;elementType&quot;: &quot;geometry.stroke&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 29 }, { &quot;weight&quot;: 0.2 } ] }, { &quot;featureType&quot;: &quot;road.arterial&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 18 } ] }, { &quot;featureType&quot;: &quot;road.local&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 16 } ] }, { &quot;featureType&quot;: &quot;transit&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 19 } ] }, { &quot;featureType&quot;: &quot;water&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 17 } ] } ]">
            <ul class="map_locations">
                <li data-y="40.643180" data-x="-73.9874068">
                    <p>9870 St Vincent Place, Glasgow, DC 45 Fr 45.</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="shell shell-wide">
        <div class="range range-xs-center range-md-justify range-lg-right range-135">
            <div class="cell-sm-8 cell-md-6 cell-lg-5">
                <div class="contact-info-wrap">
                    <h6>contact information </h6>
                    <div class="tabs-custom tabs-horizontal tabs-line tabs-line-small" id="tabs-extended-2">
                        <!-- Nav tabs-->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabs-extended-2-1" data-toggle="tab">San Diego</a></li>
                            <li><a href="#tabs-extended-2-2" data-toggle="tab">New York</a></li>
                            <li><a href="#tabs-extended-2-3" data-toggle="tab">Chicago</a></li>
                        </ul>
                        <!-- Tab panes-->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tabs-extended-2-1">
                                <ul class="list-xs list-darker">
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-map-marker"></span>
                                        <div><a href="#">2130 Fulton Street, San Diego, CA 94117-1080 USA</a></div>
                                    </li>
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-phone"></span>
                                        <ul class="list-comma">
                                            <li><a href="callto:#">1-800-1234-567</a></li>
                                            <li><a href="callto:#">1-800-6780-345</a></li>
                                        </ul>
                                    </li>
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-email-open"></span><a
                                                href="mailto:#">mail@demolink.org</a></li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="tabs-extended-2-2">
                                <ul class="list-xs list-darker">
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-map-marker"></span><a
                                                href="#">2130 Fulton Street, New York, NY 94117-1080 USA</a></li>
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-phone"></span>
                                        <ul class="list-comma">
                                            <li><a href="callto:#">1-800-0987-654</a></li>
                                            <li><a href="callto:#">1-800-0987-654</a></li>
                                        </ul>
                                    </li>
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-email-open"></span><a
                                                href="mailto:#">newyork@demolink.org</a></li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="tabs-extended-2-3">
                                <ul class="list-xs list-darker">
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-map-marker"></span><a
                                                href="#">2130 Fulton Street, Chicago, IL 94117-1080 USA</a></li>
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-phone"></span>
                                        <ul class="list-comma">
                                            <li><a href="callto:#">1-800-6543-765</a></li>
                                            <li><a href="callto:#">1-800-3434-876</a></li>
                                        </ul>
                                    </li>
                                    <li class="box-inline"><span
                                                class="icon icon-md-smaller icon-primary mdi mdi-email-open"></span><a
                                                href="mailto:#">chicago@demolink.org</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <h6>Instagram feed</h6>
                    <div class="instafeed text-center" data-instafeed-user="25025320" data-instafeed-get="user"
                         data-instafeed-sort="least-recent" data-lightgallery="group">
                        <div class="range range-narrow range-10">
                            <div class="cell-xs-3 cell-xxs-4" data-instafeed-item="">
                                <div class="thumbnail-instafeed thumbnail-instafeed-minimal"><a class="instagram-link"
                                                                                                data-lightgallery="group-item"
                                                                                                href="#"
                                                                                                data-images-standard_resolution-url="href"><img
                                                class="instagram-image" src="{{asset('images/_blank.png')}}" alt=""
                                                data-images-standard_resolution-url="src"/></a>
                                    <div class="caption">
                                        <ul class="list-inline">
                                            <li><span class="icon mdi mdi-heart"></span><span
                                                        data-likes-count="text"></span></li>
                                            <li><span class="icon mdi mdi-comment"></span><span
                                                        data-comments-count="text"></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="cell-xs-3 cell-xxs-4" data-instafeed-item="">
                                <div class="thumbnail-instafeed thumbnail-instafeed-minimal"><a class="instagram-link"
                                                                                                data-lightgallery="group-item"
                                                                                                href="#"
                                                                                                data-images-standard_resolution-url="href"><img
                                                class="instagram-image" src="{{asset('images/_blank.png')}}" alt=""
                                                data-images-standard_resolution-url="src"/></a>
                                    <div class="caption">
                                        <ul class="list-inline">
                                            <li><span class="icon mdi mdi-heart"></span><span
                                                        data-likes-count="text"></span></li>
                                            <li><span class="icon mdi mdi-comment"></span><span
                                                        data-comments-count="text"></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="cell-xs-3 cell-xxs-4" data-instafeed-item="">
                                <div class="thumbnail-instafeed thumbnail-instafeed-minimal"><a class="instagram-link"
                                                                                                data-lightgallery="group-item"
                                                                                                href="#"
                                                                                                data-images-standard_resolution-url="href"><img
                                                class="instagram-image" src="{{asset('images/_blank.png')}}" alt=""
                                                data-images-standard_resolution-url="src"/></a>
                                    <div class="caption">
                                        <ul class="list-inline">
                                            <li><span class="icon mdi mdi-heart"></span><span
                                                        data-likes-count="text"></span></li>
                                            <li><span class="icon mdi mdi-comment"></span><span
                                                        data-comments-count="text"></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="cell-xs-3 cell-xxs-4" data-instafeed-item="">
                                <div class="thumbnail-instafeed thumbnail-instafeed-minimal"><a class="instagram-link"
                                                                                                data-lightgallery="group-item"
                                                                                                href="#"
                                                                                                data-images-standard_resolution-url="href"><img
                                                class="instagram-image" src="{{asset('images/_blank.png')}}" alt=""
                                                data-images-standard_resolution-url="src"/></a>
                                    <div class="caption">
                                        <ul class="list-inline">
                                            <li><span class="icon mdi mdi-heart"></span><span
                                                        data-likes-count="text"></span></li>
                                            <li><span class="icon mdi mdi-comment"></span><span
                                                        data-comments-count="text"></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell-sm-8 cell-md-5 cell-lg-3">
                <h6>recent blog posts</h6>
                <div class="blog-inline-wrap">
                    <!-- Blog Inline-->
                    <article class="blog-inline">
                        <div class="blog-inline-title"><a href="single-post.html">How to Turn Small Talk Into Smart
                                Conversation</a></div>
                        <ul class="blog-inline-meta">
                            <li><a href="single-post.html"> 2 days ago</a></li>
                            <li>
                                by&nbsp;<a class="text-normal" href="single-post.html">Ronald Chen</a></li>
                        </ul>
                    </article>
                    <!-- Blog Inline-->
                    <article class="blog-inline">
                        <div class="blog-inline-title"><a href="single-post.html">The Top 5 Reasons Why ‘The Customer Is
                                Always Right’ Is Wrong</a></div>
                        <ul class="blog-inline-meta">
                            <li><a href="single-post.html"> 2 days ago</a></li>
                            <li>
                                by&nbsp;<a class="text-normal" href="single-post.html">Ronald Chen</a></li>
                        </ul>
                    </article>
                    <!-- Blog Inline-->
                    <article class="blog-inline">
                        <div class="blog-inline-title"><a href="single-post.html">Think You’re Too Old to Be an
                                Entrepreneur? Think Again.</a></div>
                        <ul class="blog-inline-meta">
                            <li><a href="single-post.html"> 2 days ago</a></li>
                            <li>
                                by&nbsp;<a class="text-normal" href="single-post.html">Ronald Chen</a></li>
                        </ul>
                    </article>
                    <!-- Blog Inline-->
                    <article class="blog-inline">
                        <div class="blog-inline-title"><a href="single-post.html">Why Google Doesn’t Care About College
                                Degrees</a></div>
                        <ul class="blog-inline-meta">
                            <li><a href="single-post.html"> 2 days ago</a></li>
                            <li>
                                by&nbsp;<a class="text-normal" href="single-post.html">Ronald Chen</a></li>
                        </ul>
                    </article>
                </div>
            </div>
            <div class="cell-lg-8">
                <div class="range range-md-reverse range-xs-center range-md-middle range-135 range-lg-left">
                    <div class="cell-sm-10 cell-md-5 cell-lg-5 cell-xl-5 text-center">
                        <ul class="group-xs group-middle social-icons-list">
                            <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-facebook"
                                   href="#"></a></li>
                            <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-twitter"
                                   href="#"></a></li>
                            <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-instagram"
                                   href="#"></a></li>
                            <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-google"
                                   href="#"></a></li>
                            <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-linkedin"
                                   href="#"></a></li>
                        </ul>
                    </div>
                    <div class="cell-sm-10 cell-md-7 cell-lg-7 cell-xl-6">
                        <p class="right">&#169;&nbsp;<span class="copyright-year"></span> All Rights Reserved&nbsp;<a
                                    href="#">Terms of Use</a>&nbsp;and&nbsp;<a href="privacy-policy.html">Privacy
                                Policy</a></p>
                        More <a rel="nofollow" href="http://www.templatemonster.com/category.php?category=347&type=1"
                                target="_blank">Web Development Templates at TemplateMonster.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>