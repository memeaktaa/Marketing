@extends('layouts.app')
    @section('title','403')
@section('content')
    <section class="section fullwidth-page bg-gradient-3">
        <div class="fullwidth-page-inner">
            <div class="section-md text-center">
                <div class="shell-wide">
                    <p class="heading-1 breadcrumbs-custom-title">403</p>
                </div>
            </div>
        </div>
    </section>
    @endsection