<section class="section section-pre-way-point">
    <div class="bg-white">
        <div class="shell-custom">
            <div class="range range-sm-center range-md-left range-md-middle">
                <div class="cell-sm-10 cell-md-7 cell-lg-6">
                    <div class="section-md">
                        <div class="shell-custom-inner">
                            <h3>{{trans('app.newsletter')}}</h3>
                            <hr class="divider divider-left divider-default">
                            <p>{{trans('app.letter')}}</p>
                            <!-- RD Mailform: Subscribe-->
                            <form class="rd-mailform rd-mailform-inline rd-mailform-sm rd-mailform-inline-modern" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                                <div class="rd-mailform-inline-inner">
                                    <div class="form-wrap">
                                        <input class="form-input" type="email" name="email" data-constraints="@Email @Required" id="subscribe-form-email-1"/>
                                        <label class="form-label" for="subscribe-form-email-1">{{trans('app.enter_email')}}</label>
                                    </div>
                                    <button class="button form-button button-sm button-secondary button-nina" type="submit">{{trans('app.subscribe')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="cell-lg-6">
                    <div class="range range-condensed" data-lightgallery="group">
                        <div class="cell-sm-6"><a class="gallery-item" href="images/gallery-06-original.jpg" data-lightgallery="group-item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/inline-gallery-1-480x308.jpg" alt="" width="480" height="308"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">photo #1</p>
                                        <p class="caption-text">Brave is a perfect multipurpose HTML solution designed to be used everywhere.</p>
                                    </div>
                                </div></a>
                        </div>
                        <div class="cell-sm-6"><a class="gallery-item" href="images/gallery-04-original.jpg" data-lightgallery="group-item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/inline-gallery-2-480x308.jpg" alt="" width="480" height="308"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">photo #2</p>
                                        <p class="caption-text">Brave is a perfect multipurpose HTML solution designed to be used everywhere.</p>
                                    </div>
                                </div></a>
                        </div>
                        <div class="cell-sm-6"><a class="gallery-item" href="images/gallery-05-original.jpg" data-lightgallery="group-item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/inline-gallery-3-480x308.jpg" alt="" width="480" height="308"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">photo #3</p>
                                        <p class="caption-text">Brave is a perfect multipurpose HTML solution designed to be used everywhere.</p>
                                    </div>
                                </div></a>
                        </div>
                        <div class="cell-sm-6"><a class="gallery-item" href="images/gallery-03-original.jpg" data-lightgallery="group-item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/inline-gallery-4-480x308.jpg" alt="" width="480" height="308"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">photo #4</p>
                                        <p class="caption-text">Brave is a perfect multipurpose HTML solution designed to be used everywhere.</p>
                                    </div>
                                </div></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>