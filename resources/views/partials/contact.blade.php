<section class="section section-md bg-gray-darker text-center text-sm-left">
    <div class="shell-wide">
        <div class="range range-50 range-xs-center">
            <div class="cell-xl-8">
                <div class="box-cta box-cta-inline">
                    <div class="box-cta-inner">
                        <h3 class="box-cta-title">{{trans('app.consolution')}}</h3>
                        <p>{{trans('app.contact_content')}}</p>
                    </div>
                    <div class="box-cta-inner"><a class="button button-default-outline button-nina" href="{{route('page',['page'=>'contact'])}}">{{trans('app.learn_more')}}</a></div>
                </div>
            </div>
        </div>
    </div>
</section>