<footer class="section page-footer page-footer-extended text-center text-md-left bg-gray-darker" style="height: 400px">
    <div class="rd-google-map-wrap">
        <div class="rd-google-map rd-google-map-novi rd-google-map__model" data-zoom="15" data-y="40.643180"
             data-x="-73.9874068"
             data-styles="[ { &quot;featureType&quot;: &quot;all&quot;, &quot;elementType&quot;: &quot;labels.text.fill&quot;, &quot;stylers&quot;: [ { &quot;saturation&quot;: 36 }, { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 40 } ] }, { &quot;featureType&quot;: &quot;all&quot;, &quot;elementType&quot;: &quot;labels.text.stroke&quot;, &quot;stylers&quot;: [ { &quot;visibility&quot;: &quot;on&quot; }, { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 16 } ] }, { &quot;featureType&quot;: &quot;all&quot;, &quot;elementType&quot;: &quot;labels.icon&quot;, &quot;stylers&quot;: [ { &quot;visibility&quot;: &quot;off&quot; } ] }, { &quot;featureType&quot;: &quot;administrative&quot;, &quot;elementType&quot;: &quot;geometry.fill&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 20 } ] }, { &quot;featureType&quot;: &quot;administrative&quot;, &quot;elementType&quot;: &quot;geometry.stroke&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 17 }, { &quot;weight&quot;: 1.2 } ] }, { &quot;featureType&quot;: &quot;landscape&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 20 } ] }, { &quot;featureType&quot;: &quot;poi&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 21 } ] }, { &quot;featureType&quot;: &quot;road.highway&quot;, &quot;elementType&quot;: &quot;geometry.fill&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 17 } ] }, { &quot;featureType&quot;: &quot;road.highway&quot;, &quot;elementType&quot;: &quot;geometry.stroke&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 29 }, { &quot;weight&quot;: 0.2 } ] }, { &quot;featureType&quot;: &quot;road.arterial&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 18 } ] }, { &quot;featureType&quot;: &quot;road.local&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 16 } ] }, { &quot;featureType&quot;: &quot;transit&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 19 } ] }, { &quot;featureType&quot;: &quot;water&quot;, &quot;elementType&quot;: &quot;geometry&quot;, &quot;stylers&quot;: [ { &quot;color&quot;: &quot;#000000&quot; }, { &quot;lightness&quot;: 17 } ] } ]">
            <ul class="map_locations">
                <li data-y="40.643180" data-x="-73.9874068">
                    <p>{{$address?$address:'9870 St Vincent Place, Glasgow, DC 45 Fr 45.'}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="shell shell-wide">
        <div class="range range-xs-center range-md-justify range-lg-right range-135">
            <div class="cell-sm-8 cell-md-6 cell-lg-5">
                <div class="contact-info-wrap">
                    <ul class="list-xs list-darker">
                        <li class="box-inline"><span
                                    class="icon icon-md-smaller icon-primary mdi mdi-plus"></span>
                            <div><a href="{{localizeURL('privacy')}}">{{trans('app.privacy')}}</a></div>
                        </li>
                        <li class="box-inline"><span
                                    class="icon icon-md-smaller icon-primary mdi mdi-plus"></span>
                            <div><a href="{{localizeURL('careers')}}">{{trans('app.careers')}}</a></div>
                        </li>
                        <li class="box-inline"><span
                                    class="icon icon-md-smaller icon-primary mdi mdi-plus"></span>
                            <div><a href="{{localizeURL('team')}}">{{trans('app.team')}}</a></div>
                        </li>
                    </ul>
                </div>
                <h6>{{trans('app.instagram_feed')}}</h6>
                <div class="instafeed text-center" data-instafeed-user="25025320" data-instafeed-get="user"
                     data-instafeed-sort="least-recent" data-lightgallery="group">
                    <div class="range range-narrow range-10">
                        <div class="cell-xs-3 cell-xxs-4" data-instafeed-item="">
                            <div class="thumbnail-instafeed thumbnail-instafeed-minimal"><a class="instagram-link"
                                                                                            data-lightgallery="group-item"
                                                                                            href="#"
                                                                                            data-images-standard_resolution-url="href"><img
                                            class="instagram-image" src="{{asset('images/_blank.png')}}" alt=""
                                            data-images-standard_resolution-url="src"/></a>
                                <div class="caption">
                                    <ul class="list-inline">
                                        <li><span class="icon mdi mdi-heart"></span><span
                                                    data-likes-count="text"></span></li>
                                        <li><span class="icon mdi mdi-comment"></span><span
                                                    data-comments-count="text"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cell-xs-3 cell-xxs-4" data-instafeed-item="">
                            <div class="thumbnail-instafeed thumbnail-instafeed-minimal"><a class="instagram-link"
                                                                                            data-lightgallery="group-item"
                                                                                            href="#"
                                                                                            data-images-standard_resolution-url="href"><img
                                            class="instagram-image" src="{{asset('images/_blank.png')}}" alt=""
                                            data-images-standard_resolution-url="src"/></a>
                                <div class="caption">
                                    <ul class="list-inline">
                                        <li><span class="icon mdi mdi-heart"></span><span
                                                    data-likes-count="text"></span></li>
                                        <li><span class="icon mdi mdi-comment"></span><span
                                                    data-comments-count="text"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cell-xs-3 cell-xxs-4" data-instafeed-item="">
                            <div class="thumbnail-instafeed thumbnail-instafeed-minimal"><a class="instagram-link"
                                                                                            data-lightgallery="group-item"
                                                                                            href="#"
                                                                                            data-images-standard_resolution-url="href"><img
                                            class="instagram-image" src="{{asset('images/_blank.png')}}" alt=""
                                            data-images-standard_resolution-url="src"/></a>
                                <div class="caption">
                                    <ul class="list-inline">
                                        <li><span class="icon mdi mdi-heart"></span><span
                                                    data-likes-count="text"></span></li>
                                        <li><span class="icon mdi mdi-comment"></span><span
                                                    data-comments-count="text"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cell-xs-3 cell-xxs-4" data-instafeed-item="">
                            <div class="thumbnail-instafeed thumbnail-instafeed-minimal"><a class="instagram-link"
                                                                                            data-lightgallery="group-item"
                                                                                            href="#"
                                                                                            data-images-standard_resolution-url="href"><img
                                            class="instagram-image" src="{{asset('images/_blank.png')}}" alt=""
                                            data-images-standard_resolution-url="src"/></a>
                                <div class="caption">
                                    <ul class="list-inline">
                                        <li><span class="icon mdi mdi-heart"></span><span
                                                    data-likes-count="text"></span></li>
                                        <li><span class="icon mdi mdi-comment"></span><span
                                                    data-comments-count="text"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cell-sm-8 cell-md-5 cell-lg-3">
            <h6>{{trans('app.recent_blog_posts')}}</h6>
            <div class="blog-inline-wrap">
                @foreach($blog4 as $item)
                    <article class="post-minimal">
                        <p class="post-minimal-title"><a
                                    href="{{localizeURL('blogs/'.$item->blog_id)}}">{{$item->title}}</a></p>
                        <time class="post-minimal-time"
                              datetime="2017">{{$item->created_at->format(' j F Y')}} {{trans('app.at')}} {{$item->created_at->format('H:i:s')}}</time>
                    </article>
                @endforeach
            </div>
        </div>
        <div class="cell-lg-8">
            <div class="range range-md-reverse range-xs-center range-md-middle range-135 range-lg-left">
                <div class="cell-sm-10 cell-md-5 cell-lg-5 cell-xl-5 text-center">
                    <ul class="group-xs group-middle social-icons-list">
                        <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-facebook"
                               href="#"></a></li>
                        <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-twitter"
                               href="#"></a></li>
                        <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-instagram"
                               href="#"></a></li>
                        <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-google"
                               href="#"></a></li>
                        <li><a class="icon icon-md-smaller icon-circle icon-secondary-5-filled mdi mdi-linkedin"
                               href="#"></a></li>
                    </ul>
                </div>
                <div class="cell-sm-10 cell-md-7 cell-lg-7 cell-xl-6">
                    <p class="right">&#169;&nbsp;<span class="copyright-year"></span> {{trans('app.copyright')}}</p>
                </div>
            </div>
        </div>
    </div>
</footer>