<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Verification Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the verification broker for a verification attempt
    | has failed, such as for an invalid token or invalid user.
    |
    */

    'sent' => 'تم إرسال بريد التحقق من حسابك!',
    'token' => 'رمز التفعيل غير صالح.',
    'user' => "لايوجد مستخدم بهذا الحساب",
    'subject' => 'تفعيل حسابك على iCount',
    'verify_account'=>'تفعيل الحساب',
    'send_link'=>'إراسل بريد التفعيل',
    'welcome_to_app'=>'أهلا بك إلى عائلتنا.',
    'promo'=>'نحن نحاول أن نساهم في تأمين أفضل الإرشادات في عالم الأعمال لنهيئك أنت وشبابنا بشكل احترافي أكثر عند دخولك إلى هذا العالم.',
    'click_here_to_verify'>'إضغط هنا لتفعيل حسابك',
    'dear'=>'مرحبا',
    'dear_female'=>'عزيزتي',
    'staff'=>'فريق iCount',
    'complete_account'=>' رجاءاً فعل حسابك من خلال تأكيد حسابك الالكتروني.',
    'link_dosnt_work'=>'في حال لم يعمل الرابط السابق على متصفحك  تستطيع نسخ ولصق الرابط أدناه:'
];
