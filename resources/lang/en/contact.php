<?php

return [
    'headquarter'=>'Headquarter',
    'headquarter_address'=>'Automata4 Group<br>Worldwide Headquarter<br>c/o. Public Relations Director<br>P.O.Box: 13746<br>Aleppo - Syrian Arab Republic',
    'branche'=>'Branch',
    'branche_address'=>'Panayir, Osmangazi<br>Bursa - Turkey',
    'address' => 'Address',
    'phone' =>'Phone',
    'fax' => 'Fax',
    'for_information' => 'For More <span>Information</span>',
    'full_name'=>'Full Name',
    'email'=>'eMail',
    'country'=>'Country',
    'city'=>'City',
    'phone'=>'Phone',
    'subject'=>'Subject',
    'to'=>'To',
    'message'=>'Message',
    'send_message'=>'Send Message',
    'select_one'=>'Select One',
    
    'general' => 'General Information',
    'sales' => 'Products & Sales',
    'service' => 'Customer Service',
    'webmaster' => 'Webmaster',
    'careers' => 'Job Opportunity',
    
    
    'mail_sent'=>'Mail sent successfully!',
    'mail_not_sent'=>'eMail could not be sent due to some Unexpected Error. Please Try Again later.',

    'category'=>'Category'
];