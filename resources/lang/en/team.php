<?php

return [
    'show_all' => 'Show All',
    'management' => 'Management',
    'web' => 'Web',
    'db' => 'Database',
    'software_engineering' => 'Software Engineering',
    'our_skills' => 'Our Skills are <span>Perfect</span> in',
    'qoute' => 'TECH & SOUL TEAM, PASSION LED IT',
];