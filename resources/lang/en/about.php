<?php

return [
    'interactive_office_desc' => 'We value work ethics & comfortable environment as it helps in creating a creative think tank IT House',
    'story' => 'Automata4 Story',
    'story_desc' => 'The story of Automata4 could be told as a guy working out of a small room making it big. But, this wouldn\'t give you an accurate picture of our history.',
];