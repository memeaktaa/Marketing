<?php
return [
    '1'=>'You have got to really understand something<br> about people in today’s world:',
    '2'=>'People want to be FREE,',
    '3'=>'People want happy healthy<br> homes for their FAMILIES,',
    '4'=>'People want to fulfill their DREAMS in life.',
    '5'=>'You can CALL on IT whenever you NEED it,<br>so you can relax and enjoy life.',
    '6'=>'And you can call on US.',
    '7'=>['0'=>'While it is hard to be a PIONEER in the IT World<br>when the road had already been paved…',
              '1'=>'It’s part of our NATURE to find a UNIQUE INNOVATIVE <br>ways to help people and CREATE<br>a LASTING IMPRESSION in the IT world.',],
    '8'=>'We provide SIMPLE, ADVANCED solutions to people<br>to REALIZE their dreams.',
    '9'=>'IT for People',
];