<?php

return [
    'technology' => 'Only <span>Technology</span>',
    'did_you_hear' => 'Did <span>You</span> Hear?',
    'hidden_treasure' => 'Hidden <span>Treasure</span>',
    'we_are_automata4' => '',
    'best_solution' => '',
    'work_hour' => "Work Hour/Week",
    'lines_code' => 'Lines Code',
    'no_of_projects' => 'NO. Of Projects',
    'clients_served' => 'Client Served',
];