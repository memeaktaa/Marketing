<?php

return [
    'technology' => '<span>تكنولوجيا</span> فقط',
    'did_you_hear' => 'هل <span>تعلم؟</span>',
    'hidden_treasure' => 'أسرار <span>أتوماتا</span>',
    'we_are_automata4' => 'نحن <span>أتوماتا4</span>',
    'best_solution' => 'الحل الأفضل لمتطلباتك التقنية',
    'work_hour' => 'ساعات العمل في الأسبوع',
    'lines_code' => 'سطر برمجي',
    'no_of_projects' => 'عدد المشاريع',
    'clients_served' => 'زبائن تمت خدمتهم',
];