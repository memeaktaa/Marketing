<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends \Eloquent
{
    protected $fillable = ['num_of_btn','btn1_text','btn2_text', 'text_en', 'text_ar', 'url1','url2'];


}
