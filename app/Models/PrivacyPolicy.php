<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicy extends \Eloquent
{
    protected $fillable = ['text_en', 'text_ar'];
}
