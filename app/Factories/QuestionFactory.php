<?php

namespace App\Factories;

use App\Models\Course;
use App\Models\Product;
use Aut\DataTable\Factories\GlobalFactory;

class QuestionFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
        return \Datatable::queryConfig('questions')
            ->queryDatatable($query)
            ->queryUpdateButton('question_id')
            ->queryDeleteButton('question_id')
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('questions')
                ->addHiddenInput('question_id', 'question_id', '', true)
                ->addInputText(trans('app.question'), 'text', 'text', 'req required d:datepicker')
                ->addInputText(trans('app.answer'), 'answer', 'answer', 'req required')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([],['code'])
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
